/**
 * Задача 3.
 *
 * Напишите функцию `createArray`, которая будет создавать массив с заданными значениями.
 * Первым параметром функция принимает значение, которым заполнять массив.
 * А вторым — количество элементов, которое должно быть в массиве.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента были переданы не число, не строка, не объект и не массив;
 * - В качестве второго аргумента был передан не число.
 */

// Решение

const result = createArray('x', 5);

function createArray(item, total) {
	const array = [];

	if((typeof item !== 'number') && (typeof item !== 'string') && (typeof item !== 'object') && !Array.isArray(item) ) {
		throw new Error(`${item} should be a number or a string or an object or array`);
	}

	if(typeof total !== 'number') {
		throw new Error(`${total} should be a number`);
	}

	for(let i = 0; i < total; i++) {
		array.push(item);
	}

	return array;
}

console.log(result); // [ x, x, x, x, x ]

// exports.createArray = createArray;