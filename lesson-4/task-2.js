/**
 * Задача 5.
 *
 * Вручную создать имплементацию функции `reduce`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенные методы Array.prototype.reduce и Array.prototype.reduceRight использовать запрещено;
 * - Третий аргумент функции reduce является не обязательным;
 * - Если третий аргумент передан — он станет начальным значением аккумулятора;
 * - Если третий аргумент не передан — начальным значением аккумулятора станет первый элемент обрабатываемого массива.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция;
 */

const array = [1, 2, 3, 4, 5];
const INITIAL_ACCUMULATOR = 6;

// Решение

const result = reduce(
    array,
    (accumulator, element, index, arrayRef) => {
        console.log(`${index}:`, accumulator, element, arrayRef);

        return accumulator + element;
    },
    INITIAL_ACCUMULATOR,
);

function reduce(array, callback, initialValue) {
    let accumulator = initialValue ? initialValue : 0;

    if(!Array.isArray(array)) {
        throw new Error(`${array} should be an array`);
    }

    if(typeof callback !== 'function') {
        throw new Error(`${array} should be a function`);
    }

    for(let i = 0; i < array.length; i++) {
        accumulator = callback(accumulator, array[i], i, array);
    }

    return accumulator;
}

console.log(result); // 21

// if([1, 2, 3, 4, 5].reduce((acc, item) => acc + item) === reduce(
//     array,
//     (accumulator, element, index, arrayRef) => {
//         console.log(`${index}:`, accumulator, element, arrayRef);

//         return accumulator + element;
//     }) ) {
//     console.log('ALL OK')
// }