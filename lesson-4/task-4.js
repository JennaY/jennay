/**
 * Задача 2.
 *
 * Напишите функцию `collect`, которая будет принимать массив в качестве аргумента.
 * Возвращаемое значение функции — число.
 * Массив, который передаётся в аргументе может быть одноуровневым или многоуровневым.
 * Число, которое возвращает функция должно быть суммой всех элементов
 * на всех уровнях всех вложенных массивов.
 *
 * Если при проходе всех уровней не было найдено ни одного числа,
 * то функция должна возвращать число 0.
 *
 * Условия:
 * - Обязательно использовать встроенный метод массива reduce.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - Если на каком-то уровне было найдено не число и не массив.
 */

// Решение

function collect(arr) {
	let result = 0;
	let flattendArray = [];

	if(!Array.isArray(arr)) {
		throw new Error(`${arr} should be an array`);
	}

	flattendArray = arr.flat(Infinity);

	if(flattendArray.length) {
		result = flattendArray.reduce(function (total, amount) {
			if(typeof amount !== 'number' || !(!isNaN(amount) && typeof amount === 'number')) {
				throw new Error(`${amount} should be a number`);
			}

			return total + amount;
		}, 0);
	}

	return result;
}

const array7 = [2, 1];
console.log(collect(array7));

const array1 = [[[1, 2], [1, 2]], [[2, 1], [1, 2]]];
console.log(collect(array1)); // 12

const array2 = [[[[[1, 2]]]]];
console.log(collect(array2)); // 3

const array3 = [[[[[1, 2]]], 2], 1];
console.log(collect(array3)); // 6

const array4 = [[[[[]]]]];
console.log(collect(array4)); // 0

const array5 = [[[[[], 3]]]];
console.log(collect(array5)); // 3

const array6 = {};
console.log(collect(array6));

const array7 = ['a', 1];
console.log(collect(array7));

// exports.collect = collect;


// метод flat раскрывает массивы
// result = flattendArray.reduce((total, amount) => total + amount);

// Замыкание
// function flatten(array) {
// 	var flattend = [];

// 	(function flat(array) {
// 		array.forEach(function(item) {
// 			if (Array.isArray(item)) {
// 				flat(item);
// 			} else {
// 				if(isNaN(item)) {
// 					throw new Error(`${item} should be a number`);
// 				}

// 				flattend.push(item);
// 			}
// 		});
// 	})(array);

// 	return flattend;
// }