/**
 * Напишите функцию для форматирования даты.
 * 
 * Фукнция принимает 3 аргумента
 * 1. Дата которую необходимо отформатировать
 * 2. Строка которая содержит желаемый формат даты
 * 3. Разделитель для отформтированной даты
 * 
 * Обратите внимание!
 * 1. DD день в формате — 01, 02...31
 * 2. MM месяц в формате — 01, 02...12
 * 3. YYYY год в формате — 2020, 2021...
 * 4. Строка которая обозначает формат даты разделена пробелами
 * 5. В качестве разделителя может быть передано только дефис, точка или слеш
 * 6. Генерировать ошибку если в формате даты присутствет что-то другое кроме DD, MM, YYYY
 * 7. 3-й аргумент опциональный, если он передан не был, то в качестве разделителя используется точка
*/

const formatDate = (date, format, delimiter = '.') => {
    let d = date;
    const result = [];
    const delimiterUnicode = delimiter.length === 1 ? delimiter.charCodeAt(0) : false;
    const formatArray = format.split(' ');

    if(!delimiterUnicode || delimiterUnicode < 45 || delimiterUnicode > 47) { // 45 <= delimiter.charCodeAt(0) <= 47
        throw new Error(`${delimiter} should be "." or "-" or "/"`);
    }

    for(let i = 0; i < formatArray.length; i++) {
        switch(formatArray[i]) {
            case 'DD':
                result.push(("0" + d.getDate()).slice(-2));
                break;

            case 'MM':
                result.push(("0" + d.getMonth()).slice(-2));
                break;

            case 'YYYY':
                result.push(d.getFullYear());
                break;

            default:
                throw new Error(`${formatArray[i]} is wrong. Please use correct data`);
        }
    }

    return result.join(delimiter);
};

console.log(formatDate(new Date(2021, 10, 22), 'DD MM YYYY', '/')); // 22/10/2021
console.log(formatDate(new Date(2021, 10, 22), 'DD MM', '.')); // 22.10
console.log(formatDate(new Date(2021, 10, 22), 'YYYY', '.')); // 2021
