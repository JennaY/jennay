/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

// РЕШЕНИЕ

// console.log(f2(2)); // 8

// function f(num) {
//   if(typeof num == 'number') {
//     return num ** 3;
//   } else {
//     throw new Error(`${num} is not a number`);
//   }
// }

const f = (num) => {
  if(typeof num === 'number') {
    return num ** 3;
  } else {
    throw new Error(`${num} is not a number`);
  }
}

console.log(f(2));
