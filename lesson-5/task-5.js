/**
 * Задача 3.
 *
 * Создайте функцию createFibonacciGenerator().
 *
 * Вызов функции createFibonacciGenerator() должен возвращать объект, который содержит два метода:
 * - print — возвращает число из последовательности Фибоначчи;
 * - reset — обнуляет последовательность и ничего не возвращает.
 *
 * Условия:
 * - Задачу нужно решить с помощью замыкания.
 */

// const generator1 = createFibonacciGenerator();

// РЕШЕНИЕ

function createFibonacciGenerator() {
  let count = 1;

  function getFibonacciNumbers(n) {
    if(n <= 1) {
      return n;
    } else {
      return getFibonacciNumbers(n - 1) + getFibonacciNumbers(n - 2);
    }
  }

  return {
    print() {
      const result = getFibonacciNumbers(count);
      count++;

      return result;

      // return getFibonacciNumbers(count++);
    },
    reset() {
      count = 1;
    }
  }
  
}


const generator1 = createFibonacciGenerator();

console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2
console.log(generator1.print()); // 3
console.log(generator1.reset()); // undefined
console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2

const generator2 = createFibonacciGenerator();

console.log(generator2.print()); // 1
console.log(generator2.print()); // 1
console.log(generator2.print()); // 2

// exports.createFibonacciGenerator = createFibonacciGenerator;