/**
 * Задача 2.
 *
 * Исправить функцию checkSpam(source, spam)
 * Функция возвращает true, если строка source содержит подстроку spam. Иначе — false.
 *
 * Условия:
 * - Функция принимает два параметра;
 * - Функция содержит валидацию входных параметров на тип string.
 * - Функция должна быть нечувствительна к регистру
 */

// РЕШЕНИЕ
function checkSpam(source, spam) {
    if(typeof source !== 'string' || typeof spam !== 'string') {
        console.log(`Incorrect data. ${source} and ${spam} should be strings.`);
        return;
    }

    return (source.toLowerCase().indexOf(spam.toLowerCase()) < 0) ? false : true;
}

console.log(checkSpam('pitterXXX@gmail.com', 'xxx')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'XXX')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'sss')); // false