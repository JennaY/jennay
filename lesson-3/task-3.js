/**
 * Задача 3.
 *
 * Исправить функцию truncate(string, maxLength).
 * Функция проверяет длину строки string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * Результатом функции должна быть (при необходимости) усечённая строка.
 *
 * Условия:
 * - Функция принимает два параметра;
 * - Функция содержит валидацию входных параметров;
 * - Первый параметр должен обладать типом string;
 * - Второй параметр должен обладать типом number.
 */

// РЕШЕНИЕ
function truncate(string, maxLength) {
    if(typeof string !== 'string') {
        console.log(`${string} should be a string. Please reenter.`);
        return;
    }

    if(typeof maxLength !== 'number') {
        console.log(`${maxLength} should be a number. Please reenter.`);
        return;
    }

    return string.slice(0, maxLength - 3) + '...';
}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21)); // 'Вот, что мне хотел...'