/**
 * Задача 1.
 *
 * Исправить функцию upperCaseFirst(str).
 * Функция преобразовывает первый символ переданной строки в заглавный и возвращает новую строку.
 *
 * Условия:
 * - Функция принимает один параметр;
 * - Необходимо проверить что параметр str является строкой
 */

// РЕШЕНИЕ
function upperCaseFirst(str) {
    const result = str;

    if (typeof result !== 'string') {
        return `${result} is not a string`;
    } else {
        return result.charAt(0).toUpperCase() + result.slice(1);
    }
}

console.log(upperCaseFirst('pitter')); // Pitter
console.log(upperCaseFirst('')); // ''