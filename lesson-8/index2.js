/**
 * Доработать форму из 1-го задания.
 * 
 * Добавить обработчик сабмита формы.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
*/

// РЕШЕНИЕ

document.addEventListener("DOMContentLoaded", function(){
    createForm();
    formHandle();
});

function createForm() {
    const form = document.getElementById('form');
    const button = document.createElement('button');

    createElement('input', 'Электропочта', {'type': 'email', 'class': 'form-control', 'id': 'email', 'placeholder': 'Введите свою электропочту'});
    createElement('input', 'Пароль', {'type': 'password', 'class': 'form-control', 'id': 'password', 'placeholder': 'Введите пароль'});
    createElement('input', 'Запомнить меня', {'type': 'checkbox', 'class': 'form-check-input', 'id': 'exampleCheck1'});

    button.innerHTML = 'Вход';
    setAttributes(button, {'type': 'submit', 'class': 'btn btn-primary'});

    form.append(button);

    function createElement(element, elementLabel, options) {
        const el = document.createElement(element);
        const label = document.createElement('label');
        const holder = document.createElement('div');

        setAttributes(el, options);
        label.setAttribute('for', options.id);
        label.innerHTML = elementLabel;
        holder.setAttribute('class', 'form-group');

        // if (typeof options !== 'object') {
        //     throw new Error(`${options} is incorrect. Please use object to set attributes`);
        // }

        if(options.type === 'checkbox') {
            holder.classList.add('form-check');
            label.setAttribute('class', 'form-check-label');

            holder.append(el);
            holder.append(label);
        } else {
            holder.append(label);
            holder.append(el);
        }

        form.append(holder);
    }

    function setAttributes(element, options) {
       Object.keys(options).forEach(function(attr) {
         element.setAttribute(attr, options[attr]);
       });
    }
}

function formHandle() {
    const form = document.getElementById('form');
    const regEmail = '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$';
    const email = document.getElementById('email');
    const password = document.getElementById('password');
    const checkbox = document.getElementById('exampleCheck1');
    const error = document.createElement('p');
    let obj = {};

    error.setAttribute('class', 'error');
    error.innerHTML =' ';
    form.append(error);

    form.addEventListener("submit", function (event) {
        let emailValue = email.value.trim();
        let passwordlValue = password.value.trim();

        event.preventDefault();

        // if(!emailValue || !emailValue.length) {
        //     throw new Error(`${emailValue} is empty or contains only spaces`);
        // }

        // if(!passwordlValue || !passwordlValue.length) {
        //     throw new Error(`${passwordlValue} is empty or contains only spaces`);
        // }

        if (!emailValue || !emailValue.length || !passwordlValue || !passwordlValue.length) {
            error.innerHTML = 'Пожалуйта, заполните все поля. Поля не должны содержать только пробелы.';
        } else if(!emailValue.match(regEmail)) {
            error.innerHTML = 'Неверный email. Пожалуйста, введите корректный email.';
        } else {
            error.innerHTML = '';
            obj = {
                email: emailValue,
                password: passwordlValue,
                remember: checkbox.checked,
            }
            console.log(obj);
        }
    }, false);
}
